package fr.enssat.kikeou.souleyreau_toutant

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import fr.enssat.kikeou.souleyreau_toutant.room.*
import fr.enssat.kikeou.souleyreau_toutant.viewmodels.AgendaViewModel
import fr.enssat.kikeou.souleyreau_toutant.viewmodels.AgendaViewModelFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class InformationFormFragment : Fragment() {

    internal lateinit var view: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        view = inflater.inflate(R.layout.informations_form_layout, container, false)

        val name = view.findViewById<EditText>(R.id.user_name)
        val photo = view.findViewById<EditText>(R.id.photo_url)
        val mail = view.findViewById<EditText>(R.id.email)
        val tel = view.findViewById<EditText>(R.id.phone)
        val fb = view.findViewById<EditText>(R.id.fb)

        val scope = CoroutineScope(SupervisorJob())
        val database = AgendaRoomDatabase.getDatabase(view.context, scope)
        val repository = AgendaRepository(database.agendaDao())
        val provider = ViewModelProvider(this, AgendaViewModelFactory(repository))

        // Get a new or existing ViewModel using Agenda view model factory.
        val agendaViewModel = provider.get(AgendaViewModel::class.java)

        agendaViewModel.getUser.observe(viewLifecycleOwner) { user ->
            name.setText(user.name)
            photo.setText(user.photo)
            mail.setText(user.contact.find { contact -> contact.key == "mail" }?.value)
            tel.setText(user.contact.find { contact -> contact.key == "tel" }?.value)
            fb.setText(user.contact.find { contact -> contact.key == "fb" }?.value)

            val informationNextButton = view.findViewById<Button>(R.id.information_next_button)

            informationNextButton?.setOnClickListener { _ ->
                user.name = name.text.toString()
                if (user.name != "") {
                    name.error = null

                    user.photo = photo.text.toString()

                    val contact = ArrayList<Contact>()
                    if (mail.text.toString() != "") {
                        contact.add(Contact("mail", mail.text.toString()))
                    }
                    if (tel.text.toString() != "") {
                        contact.add(Contact("tel", tel.text.toString()))
                    }
                    if (fb.text.toString() != "") {
                        contact.add(Contact("fb", fb.text.toString()))
                    }

                    user.contact = contact

                    agendaViewModel.updateUser(user)

                    val bundle = Bundle()
                    bundle.putSerializable("qrCodeInformation", user)
                    view.findNavController()
                        .navigate(R.id.action_informationFormFragment_to_calendarFormFragment,
                            bundle)
                }
                else {
                    name.requestFocus()
                    name.isSelected = true
                    name.error = view.context.resources.getString(R.string.name_required)
                }
            }
        }

        return view
    }


}