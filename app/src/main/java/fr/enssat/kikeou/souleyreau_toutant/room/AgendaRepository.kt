package fr.enssat.kikeou.souleyreau_toutant.room

import androidx.annotation.WorkerThread
import androidx.lifecycle.asLiveData
import kotlinx.coroutines.flow.Flow

class AgendaRepository(private val dao: AgendaDao) {
    val allAgendas = dao.getAlphabetizedAgendas()

    // call on a non-UI thread
    @WorkerThread
    fun insert(agenda: Agenda) {
        dao.insert(agenda)
    }

    @WorkerThread
    fun getAgendasByWeekNumber(weekNumber: Int): Flow<List<Agenda>> {
        return dao.getAgendasByWeekNumber(weekNumber)
    }

    @WorkerThread
    fun getUser(): Flow<Agenda> {
        return dao.getUser()
    }

    @WorkerThread
    fun updateUser(agenda: Agenda) {
        dao.updateUser(agenda)
    }

    @WorkerThread
    fun deleteAgenda(agenda: Agenda) {
        dao.deleteAgenda(agenda)
    }
}

