package fr.enssat.kikeou.souleyreau_toutant.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import fr.enssat.kikeou.souleyreau_toutant.databinding.CardItemBinding

class WeekListAdapter() : ListAdapter<Int, WeekViewHolder>(WeekDiff()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeekViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        var binding = CardItemBinding.inflate(inflater, parent, false)
        return WeekViewHolder.create(binding)
    }

    override fun onBindViewHolder(holder: WeekViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(current)
    }

    internal class WeekDiff : DiffUtil.ItemCallback<Int>() {
        override fun areItemsTheSame(oldItem: Int, newItem: Int): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Int, newItem: Int): Boolean {
            return oldItem == newItem
        }
    }
}