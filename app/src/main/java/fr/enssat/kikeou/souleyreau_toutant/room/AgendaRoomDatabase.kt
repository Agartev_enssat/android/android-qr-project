package fr.enssat.kikeou.souleyreau_toutant.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import fr.enssat.kikeou.souleyreau_toutant.json.ListContactConverter
import fr.enssat.kikeou.souleyreau_toutant.json.ListLocationConverter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

@TypeConverters(ListLocationConverter::class, ListContactConverter::class)
@Database(entities = [Agenda::class], version = 1, exportSchema = false)
abstract class AgendaRoomDatabase : RoomDatabase() {

        abstract fun agendaDao(): AgendaDao

        companion object {
            @Volatile
            private var INSTANCE: AgendaRoomDatabase? = null

            fun getDatabase(
                context: Context,
                scope: CoroutineScope
            ): AgendaRoomDatabase {
                // if the INSTANCE is not null, then return it,
                // if it is, then create the database
                return INSTANCE ?: synchronized(this) {
                    val aux = Room.databaseBuilder(
                        context.applicationContext,
                        AgendaRoomDatabase::class.java,
                        "agenda_database"
                    )
                        .fallbackToDestructiveMigration()
                        .addCallback(AgendaDatabaseCallback(scope))
                        .build()

                    INSTANCE = aux
                    aux
                }
            }

            private class AgendaDatabaseCallback(val scope: CoroutineScope) : RoomDatabase.Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    INSTANCE?.let { database ->
                        scope.launch(Dispatchers.IO) {
                            populate(database.agendaDao())
                        }
                    }
                }
            }

            /* populate db in coroutine*/
            fun populate(agendaDao: AgendaDao) {
                agendaDao.deleteAll()
                var contact = ArrayList<Contact>()
                var location = ArrayList<Location>()
                val user = Agenda("", "", 0,location, contact)
                agendaDao.updateUser(user)

                contact = arrayListOf(
                   Contact("mail", "guillaume.chatelet@orange.com")
                )
                location = arrayListOf(Location(2, "en TT"),Location(5, "en TT"))
                var agenda = Agenda("guillaume chatelet", "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Image_created_with_a_mobile_phone.png/640px-Image_created_with_a_mobile_phone.png", 47,location, contact)
                agendaDao.insert(agenda)

                contact = arrayListOf(
                    Contact("mail", "pierre.crepieux@orange.com")
                )
                location = arrayListOf(Location(3, "en TT"),Location(4, "en TT"))
                agenda = Agenda("pierre crepieux",  "", 47,location, contact)
                agendaDao.insert(agenda)
             }
        }
}
