package fr.enssat.kikeou.souleyreau_toutant.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import fr.enssat.kikeou.souleyreau_toutant.databinding.CardItemBinding
import fr.enssat.kikeou.souleyreau_toutant.room.Agenda

class AgendaListAdapter(): ListAdapter<Agenda, AgendaViewHolder>(AgendaDiff()) {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AgendaViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            var binding = CardItemBinding.inflate(inflater, parent, false)
            return AgendaViewHolder.create(binding)
        }

        override fun onBindViewHolder(holder: AgendaViewHolder, position: Int) {
            val current: Agenda? = getItem(position)
            holder.bind(current)
        }

        internal class AgendaDiff : DiffUtil.ItemCallback<Agenda>() {
            override fun areItemsTheSame(oldItem: Agenda, newItem: Agenda): Boolean {
                return oldItem === newItem
            }
            override fun areContentsTheSame(oldItem: Agenda, newItem: Agenda): Boolean {
                return oldItem.name.equals(newItem.name)
            }
        }
}