package fr.enssat.kikeou.souleyreau_toutant

import android.media.MediaDrm
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import fr.enssat.kikeou.souleyreau_toutant.adapters.AgendaListAdapter
import fr.enssat.kikeou.souleyreau_toutant.databinding.CalendarsDisplayLayoutBinding
import fr.enssat.kikeou.souleyreau_toutant.room.Agenda
import fr.enssat.kikeou.souleyreau_toutant.room.AgendaRepository
import fr.enssat.kikeou.souleyreau_toutant.room.AgendaRoomDatabase
import fr.enssat.kikeou.souleyreau_toutant.viewmodels.AgendaViewModel
import fr.enssat.kikeou.souleyreau_toutant.viewmodels.AgendaViewModelFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class AgendaListFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = CalendarsDisplayLayoutBinding.inflate(inflater)
        val bundle = arguments
        val weekNumber = bundle?.getSerializable("weekNumber") as Int


        val adapter = AgendaListAdapter()
        binding.agenda.adapter = adapter
        binding.agenda.layoutManager = LinearLayoutManager(context)

        val scope = CoroutineScope(SupervisorJob())
        val database = AgendaRoomDatabase.getDatabase(binding.root.context, scope)
        val repository = AgendaRepository(database.agendaDao())
        val provider = ViewModelProvider(this, AgendaViewModelFactory(repository))

        val agendaViewModel = provider.get(AgendaViewModel::class.java)
        agendaViewModel.getAgendasByWeekNumber(weekNumber).observe(viewLifecycleOwner) { agendas ->
            adapter.submitList(agendas)
        }

        return binding.root
    }
}