package fr.enssat.kikeou.souleyreau_toutant.adapters

import android.os.Bundle
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import fr.enssat.kikeou.souleyreau_toutant.R
import fr.enssat.kikeou.souleyreau_toutant.databinding.CardItemBinding
import fr.enssat.kikeou.souleyreau_toutant.room.Agenda


class AgendaViewHolder private constructor(var binding:CardItemBinding):RecyclerView.ViewHolder(binding.root) {

    fun bind(agenda: Agenda?) {
        binding.cardItemImage.setImageResource(R.drawable.person)
        binding.cardItemTitle.text = agenda?.name

        val bundle = Bundle()
        bundle.putSerializable("agenda", agenda)
        binding.root.setOnClickListener() {
            it.findNavController().navigate(R.id.action_agendaListFragment_to_agendaDisplayFragment, bundle )
        }
    }

    companion object {
        fun create(binding: CardItemBinding): AgendaViewHolder {
            return AgendaViewHolder(binding)
        }
    }
}