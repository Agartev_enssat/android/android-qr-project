package fr.enssat.kikeou.souleyreau_toutant

import android.graphics.Bitmap
import android.os.Bundle
import java.lang.Exception
import android.provider.MediaStore
import android.content.ContentValues
import android.net.Uri
import android.os.Environment
import android.view.View
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.google.android.material.floatingactionbutton.FloatingActionButton

class QrDisplayFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.qr_display_layout, container, false)

        val qrCodeView = view.findViewById<ImageView>(R.id.qr_code_view)
        val share = view.findViewById<FloatingActionButton>(R.id.share)
        val menu = view.findViewById<Button>(R.id.menu_button)

        val bundle = arguments
        val bitmap = bundle?.getParcelable<Bitmap>("qr_code")
        qrCodeView.setImageBitmap(bitmap)

        var uri : Uri? = null

        try {
            //in your android manifest
            //<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
            //to access media storage for android <10

            val values = ContentValues()
            values.put(MediaStore.MediaColumns.DISPLAY_NAME, "kikeoo_qrcode"/*QR_CODE_FILE_NAME*/)
            values.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
            values.put(
                MediaStore.MediaColumns.RELATIVE_PATH,
                Environment.DIRECTORY_DOCUMENTS.toString() + "/Kikeou/"
            )

            uri = activity?.contentResolver?.insert(MediaStore.Files.getContentUri("external"), values)
            val out = activity?.contentResolver?.openOutputStream(uri!!)
            bitmap?.compress(Bitmap.CompressFormat.JPEG, 100/*NO_COMPRESSION*/, out)
            out?.flush()
            out?.close()
            share.visibility = View.VISIBLE

            //to get png in Android Studio : View -> Tool Windows -> Device File Explorer
            //in storage/self/primary/Documents/Kikeou
            //also in sdcard/Documents/Kikeou
            //synchronize Documents folder if needed
        } catch (e: Exception) {
            e.printStackTrace()
        }

        menu?.setOnClickListener { _ ->
            view.findNavController()
                .navigate(R.id.action_qrDisplayFragment_to_mainActivity, bundle)
        }

        share.setOnClickListener { _ ->
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "image/jpeg"
            intent.putExtra(Intent.EXTRA_STREAM, uri)
            startActivity(Intent.createChooser(intent, "Share Image"))
        }

        return view
    }

}