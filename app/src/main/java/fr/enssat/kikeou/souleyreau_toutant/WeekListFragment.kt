package fr.enssat.kikeou.souleyreau_toutant

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import fr.enssat.kikeou.souleyreau_toutant.adapters.AgendaListAdapter
import fr.enssat.kikeou.souleyreau_toutant.adapters.WeekListAdapter
import fr.enssat.kikeou.souleyreau_toutant.databinding.CalendarsDisplayLayoutBinding
import fr.enssat.kikeou.souleyreau_toutant.databinding.WeeksDisplayLayoutBinding
import fr.enssat.kikeou.souleyreau_toutant.room.AgendaRepository
import fr.enssat.kikeou.souleyreau_toutant.room.AgendaRoomDatabase
import fr.enssat.kikeou.souleyreau_toutant.viewmodels.AgendaViewModel
import fr.enssat.kikeou.souleyreau_toutant.viewmodels.AgendaViewModelFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class WeekListFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val weekValues = (1..52).toList();

        val binding = WeeksDisplayLayoutBinding.inflate(inflater)
        // get the recycler view

        val adapter = WeekListAdapter()
        binding.weeks.adapter = adapter
        binding.weeks.layoutManager = LinearLayoutManager(context)

        adapter.submitList(weekValues)

        return binding.root
    }
}