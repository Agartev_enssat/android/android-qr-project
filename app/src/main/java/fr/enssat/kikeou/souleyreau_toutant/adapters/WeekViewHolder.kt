package fr.enssat.kikeou.souleyreau_toutant.adapters

import android.os.Bundle
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import fr.enssat.kikeou.souleyreau_toutant.R
import fr.enssat.kikeou.souleyreau_toutant.databinding.CardItemBinding


class WeekViewHolder private constructor(var binding: CardItemBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(weekNumber: Int) {
        val title = binding.root.context.resources.getString(R.string.week) + " " + weekNumber
        binding.cardItemTitle.text = title
        binding.cardItemImage.setImageResource(R.drawable.calendar)

        val bundle = Bundle()
        bundle.putSerializable("weekNumber", weekNumber)
        binding.root.setOnClickListener() {
            it.findNavController().navigate(R.id.action_weekListFragment_to_agendaListFragment, bundle)
        }
    }

    companion object {
        fun create(binding: CardItemBinding): WeekViewHolder {
            return WeekViewHolder(binding)
        }
    }
}