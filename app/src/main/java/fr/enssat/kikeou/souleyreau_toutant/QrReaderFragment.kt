package fr.enssat.kikeou.souleyreau_toutant

import android.os.Bundle
import android.content.Context
import android.view.View
import android.content.pm.PackageManager
import android.graphics.*
import android.util.Log
import android.util.Size
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.*
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.google.common.util.concurrent.ListenableFuture
import com.google.mlkit.common.model.LocalModel
import com.google.mlkit.vision.barcode.Barcode
import com.google.mlkit.vision.barcode.BarcodeScanner
import com.google.mlkit.vision.barcode.BarcodeScannerOptions
import com.google.mlkit.vision.barcode.BarcodeScanning
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.objects.ObjectDetection
import com.google.mlkit.vision.objects.ObjectDetector
import com.google.mlkit.vision.objects.custom.CustomObjectDetectorOptions
import com.google.zxing.BarcodeFormat
import fr.enssat.kikeou.souleyreau_toutant.databinding.QrReaderLayoutBinding
import fr.enssat.kikeou.souleyreau_toutant.json.AgendaJsonParser
import fr.enssat.kikeou.souleyreau_toutant.room.AgendaRepository
import fr.enssat.kikeou.souleyreau_toutant.room.AgendaRoomDatabase
import fr.enssat.kikeou.souleyreau_toutant.viewmodels.AgendaViewModel
import fr.enssat.kikeou.souleyreau_toutant.viewmodels.AgendaViewModelFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class QrReaderFragment : Fragment() {

    private val REQUEST_CODE = 123456

    private lateinit var binding: QrReaderLayoutBinding
    private lateinit var objectDetector: ObjectDetector
    private lateinit var cameraProviderFuture : ListenableFuture<ProcessCameraProvider>

    internal lateinit var view : View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = QrReaderLayoutBinding.inflate(inflater)

        view = binding.root

        return binding.root
    }


    override fun onRequestPermissionsResult(requestCode: Int,permissions: Array<String>, grantResults: IntArray) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        when (requestCode) {
            REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    initCameraAndTF()
                } else {
                    Toast.makeText(view.context, this.getString(R.string.permission_to_grant), Toast.LENGTH_LONG).show()
                }
                return
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (ContextCompat.checkSelfPermission(view.context, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            initCameraAndTF()
        } else {
            val permissions = arrayOf(android.Manifest.permission.CAMERA)
            ActivityCompat.requestPermissions(requireActivity(),permissions,REQUEST_CODE)
        }
    }

    //Only the original thread that created a view can touch its views.
    private fun initCameraAndTF() = binding.previewView.post {

        // tensor flow lite model to detect object in camera preview
        // model retrieve from
        // https://tfhub.dev/google/object_detection/mobile_object_labeler_v1/1
        // store in assets folder of the project
        val TFModel = LocalModel.Builder()
            .setAssetFilePath("lite-model_object_detection_mobile_object_labeler_v1_1.tflite")
            .build()

        // Detect oject according to TF model
        // Stream mode to skip image when camera move too fast
        // Threshold confidence up to 50% to classify object according to Tensor Flow model
        val customObjectDetectorOptions =
            CustomObjectDetectorOptions.Builder(TFModel)
                .setDetectorMode(CustomObjectDetectorOptions.STREAM_MODE)
                .enableClassification()
                .setClassificationConfidenceThreshold(0.5f)
                .setMaxPerObjectLabelCount(3)
                .build()
        objectDetector = ObjectDetection.getClient(customObjectDetectorOptions)

        val barcodeScanner: BarcodeScanner = BarcodeScanning.getClient(
            BarcodeScannerOptions.Builder()
                .setBarcodeFormats(Barcode.FORMAT_QR_CODE)
                .build()
        )

        val scope = CoroutineScope(SupervisorJob())
        val database = AgendaRoomDatabase.getDatabase(binding.root.context, scope)
        val repository = AgendaRepository(database.agendaDao())
        val provider = ViewModelProvider(this, AgendaViewModelFactory(repository))

        // Get a new or existing ViewModel using Agenda view model factory.
        val agendaViewModel = provider.get(AgendaViewModel::class.java)

        // future do not block
        // get() is used to get the instance of the future when available
        cameraProviderFuture = ProcessCameraProvider.getInstance(view.context)
        cameraProviderFuture.addListener(
            {
                val cameraProvider = cameraProviderFuture.get()

                // Set up the view finder use case to display camera preview
                val preview = Preview.Builder()
                    .setTargetAspectRatio(AspectRatio.RATIO_4_3)
                    .build()

                // Set up the image analysis use case which will process frames in real time
                val imageAnalysis = ImageAnalysis.Builder()
                    .setTargetResolution(Size(1080, 2340))
                    .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                    .build()

                imageAnalysis.setAnalyzer(
                    ContextCompat.getMainExecutor(view.context),
                    @androidx.camera.core.ExperimentalGetImage { imageProxy ->
                        val rotationDegrees = imageProxy.imageInfo.rotationDegrees
                        val image = imageProxy.image
                        if (image != null) {
                            val processImage = InputImage.fromMediaImage(image, rotationDegrees)

                            barcodeScanner.process(processImage)
                                .addOnFailureListener {
                                    Log.e("ScannerActivity", "Error: $it.message")
                                    imageProxy.close()
                                }
                                .addOnSuccessListener { barcodes ->
                                    var rawValue = ""
                                    for (it in barcodes) {
                                        if (!rawValue.equals(it.rawValue)) {
                                            rawValue = it.rawValue!!
                                            // process data
                                            try {
                                                Log.i("json", rawValue)
                                                val agenda = AgendaJsonParser.parseAgenda(rawValue)
                                                val bundleAgendaList = Bundle()
                                                val bundleAgendaDisplay = Bundle()
                                                agenda?.let { newAgenda ->
                                                    agendaViewModel.insertAgenda(newAgenda)
                                                    bundleAgendaList.putInt("weekNumber",
                                                        newAgenda.week)
                                                    bundleAgendaDisplay.putSerializable("agenda",
                                                        newAgenda)
                                                }
                                                Toast.makeText(context, context?.resources?.getString(R.string.qrcode_read_with_success), Toast.LENGTH_SHORT).show()
                                                view.findNavController().navigate(R.id.action_qrReaderFragment_to_weekListFragment)
                                                view.findNavController().navigate(R.id.action_weekListFragment_to_agendaListFragment, bundleAgendaList)
                                                view.findNavController().navigate(R.id.action_agendaListFragment_to_agendaDisplayFragment, bundleAgendaDisplay)
                                                break;
                                            }
                                            catch (exception: Exception) {
                                                Log.e("QrReaderFragment", exception.toString())
                                                Toast.makeText(context, context?.resources?.getString(R.string.wrong_json_format), Toast.LENGTH_SHORT).show()
                                                view.findNavController().navigateUp()
                                                break;
                                            }
                                        }
                                    }
                                    imageProxy.close()
                                }
                        }
                    }
                )

                // Create a new camera selector each time, enforcing lens facing
                val cameraSelector = CameraSelector.Builder().build()

                // Apply declared configs to CameraX using the same lifecycle owner
                cameraProvider.unbindAll()
                val camera = cameraProvider.bindToLifecycle(viewLifecycleOwner, cameraSelector, preview, imageAnalysis)

                // Use the camera object to link our preview use case with the view
                preview.setSurfaceProvider(binding.previewView.surfaceProvider)
            },
            ContextCompat.getMainExecutor(view.context)
        )
    }

}