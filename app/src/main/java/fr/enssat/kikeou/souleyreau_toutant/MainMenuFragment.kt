package fr.enssat.kikeou.souleyreau_toutant

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.app.AppLaunchChecker
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import fr.enssat.kikeou.souleyreau_toutant.databinding.MainMenuLayoutBinding
import fr.enssat.kikeou.souleyreau_toutant.room.AgendaRepository
import fr.enssat.kikeou.souleyreau_toutant.room.AgendaRoomDatabase
import fr.enssat.kikeou.souleyreau_toutant.viewmodels.AgendaViewModel
import fr.enssat.kikeou.souleyreau_toutant.viewmodels.AgendaViewModelFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class MainMenuFragment : Fragment() {

    private lateinit var binding: MainMenuLayoutBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainMenuLayoutBinding.inflate(inflater)

        // populate database on first app launch
        val hasLaunched = AppLaunchChecker.hasStartedFromLauncher(binding.root.context)
        if (!hasLaunched) {
            val scope = CoroutineScope(SupervisorJob())
            val database = AgendaRoomDatabase.getDatabase(binding.root.context, scope)
            val repository = AgendaRepository(database.agendaDao())
            val provider = ViewModelProvider(this, AgendaViewModelFactory(repository))
            val agendaViewModel = provider.get(AgendaViewModel::class.java)

            agendaViewModel.getUser.observe(viewLifecycleOwner) {}
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val generateQRCodeButton = binding.generateQrCodeButton
        generateQRCodeButton.setOnClickListener { _ ->
            view.findNavController().navigate(R.id.action_mainActivity_to_informationFormFragment)
        }

        val scanQRCodeButton = binding.scanQrCodeButton
        scanQRCodeButton.setOnClickListener { _ ->
            view.findNavController().navigate(R.id.action_mainActivity_to_qrReaderFragment)
        }

        val consultCalendarsButton = binding.consultCalendarsButton
        consultCalendarsButton.setOnClickListener { _ ->
            view.findNavController().navigate(R.id.action_mainActivity_to_weekListFragment)
        }
    }
}