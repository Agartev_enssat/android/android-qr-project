package fr.enssat.kikeou.souleyreau_toutant.json

import androidx.room.TypeConverter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import fr.enssat.kikeou.souleyreau_toutant.room.Location

class ListLocationConverter {
    private val moshi = Moshi.Builder().build()
    private val locType = Types.newParameterizedType(List::class.java, Location::class.java)
    private val locAdapter = moshi.adapter<List<Location>>(locType)

    @TypeConverter
    fun stringToLocations(string: String): List<Location> {
        return locAdapter.fromJson(string).orEmpty()
    }

    @TypeConverter
    fun locationsToString(loc: List<Location>): String {
        return locAdapter.toJson(loc)
    }

}