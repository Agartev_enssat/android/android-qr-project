package fr.enssat.kikeou.souleyreau_toutant.room

import androidx.room.*
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import fr.enssat.kikeou.souleyreau_toutant.json.ListLocationConverter
import fr.enssat.kikeou.souleyreau_toutant.json.ListContactConverter
import java.io.Serializable

@JsonClass(generateAdapter = true)
data class Location(var day:Int, var place:String)

@JsonClass(generateAdapter = true)
data class Contact(var key: String, var value: String)

@Entity(tableName = "agenda_table")
@JsonClass(generateAdapter = true)
data class Agenda(
    @ColumnInfo(name = "name")
    @field:Json(name = "name") var name: String,

    @ColumnInfo(name = "photo")
    @field:Json(name = "photo") var photo: String,

    @ColumnInfo(name = "week")
    @field:Json(name = "week") var week: Int,

    @TypeConverters(ListLocationConverter::class)
    @field:Json(name = "loc") var loc: List<Location>,

    @TypeConverters(ListContactConverter::class)
    @field:Json(name = "contact") var contact: List<Contact>
) : Serializable {

    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    @Transient
    private var id:Int = 0
        fun getId():Int {
            return id
        }
        fun  setId(value:Int){
            id = value
        }
}