package fr.enssat.kikeou.souleyreau_toutant.viewmodels

import androidx.lifecycle.*
import fr.enssat.kikeou.souleyreau_toutant.room.Agenda
import fr.enssat.kikeou.souleyreau_toutant.room.AgendaRepository
import kotlinx.coroutines.launch
import androidx.lifecycle.asLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class AgendaViewModel (private val repository: AgendaRepository) : ViewModel() {

    // update UI when agendas datachanges.
    // Repository separated from UI through ViewModel.
    val allAgendas = repository.allAgendas.asLiveData()
    val getUser = repository.getUser().asLiveData()

    private val scope = CoroutineScope(SupervisorJob())

    /**
     * insert the data in non-blocking coroutine
     */
    fun insertAgenda(agd: Agenda) = scope.launch {
        repository.insert(agd)
    }

    fun getAgendasByWeekNumber(weekNumber: Int): LiveData<List<Agenda>> {
        return repository.getAgendasByWeekNumber(weekNumber).asLiveData()
    }

    fun updateUser(user: Agenda) = scope.launch {
        repository.updateUser(user)
    }

    fun deleteAgenda(agenda: Agenda) = scope.launch {
        repository.deleteAgenda(agenda)
    }
}
