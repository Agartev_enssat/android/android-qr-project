package fr.enssat.kikeou.souleyreau_toutant

import android.content.Context
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import fr.enssat.kikeou.souleyreau_toutant.room.Agenda
import androidx.recyclerview.widget.LinearLayoutManager
import fr.enssat.kikeou.souleyreau_toutant.adapters.AgendaDisplayListAdapter
import fr.enssat.kikeou.souleyreau_toutant.databinding.AgendaDisplayLayoutBinding
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.widget.ImageView
import java.net.URL
import android.os.AsyncTask
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import fr.enssat.kikeou.souleyreau_toutant.room.AgendaRepository
import fr.enssat.kikeou.souleyreau_toutant.room.AgendaRoomDatabase
import fr.enssat.kikeou.souleyreau_toutant.viewmodels.AgendaViewModel
import fr.enssat.kikeou.souleyreau_toutant.viewmodels.AgendaViewModelFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class AgendaDisplayFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = AgendaDisplayLayoutBinding.inflate(inflater, container, false)

        val bundle = arguments
        val agenda = bundle?.getSerializable("agenda") as Agenda

        // display collaborator information
        binding.userName.text = agenda.name
        val photoImageView = binding.collaboratorImageView
        photoImageView.setImageResource(R.drawable._024px_no_image_available_svg)
        DownloadImageTask(photoImageView).execute(agenda.photo)

        val contactAdapter = AgendaDisplayListAdapter()
        binding.contactCLayout.adapter = contactAdapter
        binding.contactCLayout.layoutManager = LinearLayoutManager(context)

        contactAdapter.submitList(agenda.contact)

        val locationAdapter = AgendaDisplayListAdapter()
        binding.locationCLayout.adapter = locationAdapter
        binding.locationCLayout.layoutManager = LinearLayoutManager(context)

        locationAdapter.submitList(agenda.loc)

        val scope = CoroutineScope(SupervisorJob())
        val database = AgendaRoomDatabase.getDatabase(binding.root.context, scope)
        val repository = AgendaRepository(database.agendaDao())
        val provider = ViewModelProvider(this, AgendaViewModelFactory(repository))

        val agendaViewModel = provider.get(AgendaViewModel::class.java)

        binding.removeInformation.setOnClickListener {
            agendaViewModel.deleteAgenda(agenda)
            binding.root.findNavController().popBackStack()
        }

        return binding.root
    }
}

private class DownloadImageTask(val bmImage: ImageView) :
    AsyncTask<String?, Void?, Bitmap?>() {
    override fun doInBackground(vararg params: String?): Bitmap? {
        val urldisplay = params[0]
        var mIcon11: Bitmap? = null
        try {
            val url = URL(urldisplay).openStream()
            mIcon11 = BitmapFactory.decodeStream(url)
        } catch (e: Exception) {
            Log.e("Error", e.message!!)
            e.printStackTrace()
        }
        return mIcon11
    }

    override fun onPostExecute(result: Bitmap?) {
        if(result != null) {
            bmImage.setImageBitmap(result)
        }
    }
}