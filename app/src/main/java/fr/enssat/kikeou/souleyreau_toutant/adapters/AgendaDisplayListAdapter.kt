package fr.enssat.kikeou.souleyreau_toutant.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import fr.enssat.kikeou.souleyreau_toutant.databinding.AgendaDisplayRecyclerViewBinding
import fr.enssat.kikeou.souleyreau_toutant.room.Contact
import fr.enssat.kikeou.souleyreau_toutant.room.Location

class AgendaDisplayListAdapter(): ListAdapter<Any, AgendaDisplayViewHolder>(AgendaDiff()) {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AgendaDisplayViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val binding = AgendaDisplayRecyclerViewBinding.inflate(inflater, parent, false)
            return AgendaDisplayViewHolder.create(binding)
        }

        override fun onBindViewHolder(holder: AgendaDisplayViewHolder, position: Int) {
            val current = getItem(position)
            if (current is Contact) {
                holder.bindContact(current as Contact)
            }
            else {
                holder.bindLocation(current as Location)
            }
        }

        internal class AgendaDiff : DiffUtil.ItemCallback<Any>() {
            override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
                return oldItem === newItem
            }

            override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean {
                TODO("Not yet implemented")
            }
        }
}