package fr.enssat.kikeou.souleyreau_toutant.json

import androidx.room.TypeConverter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import fr.enssat.kikeou.souleyreau_toutant.room.Contact

class ListContactConverter {
    private val moshi = Moshi.Builder().build()
    private val contactType = Types.newParameterizedType(List::class.java, Contact::class.java)
    private val contactAdapter = moshi.adapter<List<Contact>>(contactType)

    @TypeConverter
    fun stringToContacts(string: String): List<Contact> {
        return contactAdapter.fromJson(string).orEmpty()
    }

    @TypeConverter
    fun contactsToString(contact: List<Contact>): String {
        return contactAdapter.toJson(contact)
    }

}