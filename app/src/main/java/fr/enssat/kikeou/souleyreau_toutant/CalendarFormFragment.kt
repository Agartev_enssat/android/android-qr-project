package fr.enssat.kikeou.souleyreau_toutant

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import fr.enssat.kikeou.souleyreau_toutant.json.AgendaJsonParser
import fr.enssat.kikeou.souleyreau_toutant.room.Agenda
import fr.enssat.kikeou.souleyreau_toutant.room.Location
import java.util.*
import net.glxn.qrgen.android.QRCode
import kotlin.collections.ArrayList

class CalendarFormFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val bundle = arguments
        val qrCodeInformation = bundle?.getSerializable("qrCodeInformation") as Agenda
        val view = inflater.inflate(R.layout.calendar_form_layout, container, false)

        val locationsIds = intArrayOf(
            R.id.monday,
            R.id.tuesday_location,
            R.id.wednesday_location,
            R.id.thursday_location,
            R.id.friday_location,
            R.id.saturday_location,
            R.id.sunday_location
        )

        for (id in locationsIds) {
            val autoCompleteTextView = view.findViewById<AutoCompleteTextView>(id)
//            Log.i("CalendarFormFragment", autoCompleteTextView.toString())
            val adapter = ArrayAdapter(view.context,android.R.layout.simple_list_item_1, resources.getStringArray(R.array.locations))
            autoCompleteTextView.threshold = 1
            autoCompleteTextView.setAdapter(adapter)
        }
        view.findViewById<AutoCompleteTextView>(R.id.saturday_location)
            .setText(resources.getStringArray(R.array.locations)[4])
        view.findViewById<AutoCompleteTextView>(R.id.sunday_location)
            .setText(resources.getStringArray(R.array.locations)[4])

        val calendar = Calendar.getInstance()
        val weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR)

        val week = view.findViewById<TextView>(R.id.weekChoice)
        week.text = weekOfYear.toString()
        fun validWeekNumber(weekNumber: Int): Int {
            if (weekNumber > 52) {
                return 1
            }
            if (weekNumber + 1 == weekOfYear) {
                return weekOfYear
            }
            if (weekNumber < 1) {
                return 52
            }

            return weekNumber
        }

        val selectPreviousWeek = view.findViewById<Button>(R.id.selectPreviousWeek)
        selectPreviousWeek?.isEnabled = false
        selectPreviousWeek?.setTextColor(ContextCompat.getColor(view.context, R.color.white))
        selectPreviousWeek?.setBackgroundColor(Color.GRAY)
        val selectNextWeek = view.findViewById<Button>(R.id.selectNextWeek)

        selectPreviousWeek.setOnClickListener { _ ->
            var previousWeek = Integer.parseInt(week.text.toString()) - 1
            previousWeek = validWeekNumber(previousWeek)
            if (previousWeek == weekOfYear) {
                selectPreviousWeek?.isEnabled = false
                selectPreviousWeek?.setBackgroundColor(Color.GRAY)
            }
            week.text = previousWeek.toString()
        }
        selectNextWeek.setOnClickListener { _ ->
            var nextWeek = Integer.parseInt(week.text.toString()) + 1
            nextWeek = validWeekNumber(nextWeek)
            week.text = nextWeek.toString()
            if (!selectPreviousWeek.isEnabled) {
                selectPreviousWeek?.isEnabled = true
                selectPreviousWeek?.setBackgroundColor(
                    ContextCompat.getColor(
                        view.context,
                        R.color.kikeoo_primary
                    )
                )
            }
        }

        initQRGeneration(view, qrCodeInformation, locationsIds)

        return view
    }

    private fun initQRGeneration(
        view: View,
        qrCodeInformation: Agenda,
        locationIds: IntArray
    ) {
        val button = view.findViewById<Button>(R.id.generate_qr_code_button2)

        button.setOnClickListener { _ ->

            val locations = ArrayList<Location>()

            for (i in locationIds.indices) {
                val locationText = view.findViewById<AutoCompleteTextView>(locationIds[i]).text
                if (locationText.isNotEmpty()) {
                    val location = Location(
                        i,
                        locationText.toString()
                    )
                    locations.add(location)
                }
            }
            qrCodeInformation.loc = locations

            qrCodeInformation.week = Integer.parseInt(view.findViewById<TextView>(R.id.weekChoice).text.toString())

            // use moshi to convert the class in json
            val json: String = AgendaJsonParser.parseAgenda(qrCodeInformation)
            val bitmap: Bitmap = QRCode.from(json).withSize(300,300).bitmap()
            val bundle = Bundle()
            bundle.putParcelable("qr_code", bitmap)
            Log.println(Log.DEBUG, "json", json)
            view.findNavController()
                .navigate(R.id.action_calendarFormFragment_to_qrDisplayFragment, bundle)
        }

    }

}