package fr.enssat.kikeou.souleyreau_toutant.room

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface AgendaDao {
    // allowing the insert of agenda with conflict resolution strategy
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(agenda: Agenda)

    @Query("DELETE FROM agenda_table")
    fun deleteAll()

    @Query("SELECT * FROM agenda_table WHERE id = 1")
    fun getUser(): Flow<Agenda>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateUser(agenda: Agenda)

    @Query("SELECT * FROM agenda_table ORDER BY name ASC")
    fun getAlphabetizedAgendas(): Flow<List<Agenda>>

    @Query("SELECT * FROM agenda_table WHERE week = :weekNumber ORDER BY name ASC")
    fun getAgendasByWeekNumber(weekNumber: Int): Flow<List<Agenda>>

    @Delete
    fun deleteAgenda(agenda: Agenda)
}

