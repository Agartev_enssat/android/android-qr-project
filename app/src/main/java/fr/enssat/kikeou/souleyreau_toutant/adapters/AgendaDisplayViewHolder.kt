package fr.enssat.kikeou.souleyreau_toutant.adapters

import androidx.recyclerview.widget.RecyclerView
import fr.enssat.kikeou.souleyreau_toutant.R
import fr.enssat.kikeou.souleyreau_toutant.databinding.AgendaDisplayRecyclerViewBinding
import fr.enssat.kikeou.souleyreau_toutant.room.Contact
import fr.enssat.kikeou.souleyreau_toutant.room.Location

class AgendaDisplayViewHolder private constructor(var binding: AgendaDisplayRecyclerViewBinding):RecyclerView.ViewHolder(binding.root) {

    fun bindContact(contact: Contact) {
        binding.placeholder.text = contact.key.capitalize()
        binding.value.text = contact.value
        binding.value.setTextIsSelectable(true)
    }

    fun bindLocation(location: Location) {
        binding.placeholder.text = binding.root.context.resources.getStringArray(R.array.week)[location.day]
        binding.value.text = location.place
        binding.value.setTextIsSelectable(true)
    }

    companion object {
        fun create(binding: AgendaDisplayRecyclerViewBinding): AgendaDisplayViewHolder {
            return AgendaDisplayViewHolder(binding)
        }
    }
}