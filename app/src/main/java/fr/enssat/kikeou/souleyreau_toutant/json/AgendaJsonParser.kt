package fr.enssat.kikeou.souleyreau_toutant.json

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import fr.enssat.kikeou.souleyreau_toutant.room.Agenda

class AgendaJsonParser {

    companion object {
        private val moshi: Moshi = Moshi.Builder().build()
        private val adapter: JsonAdapter<Agenda> = moshi.adapter(Agenda::class.java)

        fun parseAgenda(json: String): Agenda? {
            return adapter.fromJson(json)
        }

        fun parseAgenda(agenda: Agenda): String {
            return adapter.toJson(agenda)
        }
    }

}